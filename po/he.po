# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-01-24 11:25+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/libertine-manager-app.desktop.in:4
msgid "Libertine Manager"
msgstr ""

#: data/libertine-manager-app.desktop.in:5
#: data/libertine-manager-app.desktop.in:11
msgid "X11 Application Sandbox"
msgstr ""

#: common/ContainerManager.cpp:26
msgid "%1 failed to start"
msgstr ""

#: common/ContainerManager.cpp:27
msgid "Installation of packages failed"
msgstr ""

#: common/ContainerManager.cpp:28
msgid "Removal of packages failed"
msgstr ""

#: common/ContainerManager.cpp:29
msgid "Searching for query %1 failed"
msgstr ""

#: common/ContainerManager.cpp:30
msgid "Updating container %1 failed"
msgstr ""

#: common/ContainerManager.cpp:31
msgid "Creating container %1 failed"
msgstr ""

#: common/ContainerManager.cpp:32
msgid "Destroying container %1 failed"
msgstr ""

#: common/ContainerManager.cpp:33
msgid "Running command %1 failed"
msgstr ""

#: common/ContainerManager.cpp:34
msgid "Attempt to configure container %1 failed"
msgstr ""

#: common/ContainerManager.cpp:35
msgid "Attempt to set container as default failed"
msgstr ""

#: common/ContainerManager.cpp:36
msgid "An error occurred"
msgstr ""

#: common/ContainersConfig.cpp:25
msgid "new"
msgstr ""

#: common/ContainersConfig.cpp:26 qml/common/ContainerEditView.qml:241
#: qml/common/ContainersList.qml:67 qml/common/ExtraArchivesView.qml:68
msgid "installing"
msgstr ""

#: common/ContainersConfig.cpp:27
msgid "installed"
msgstr ""

#: common/ContainersConfig.cpp:28 qml/common/ContainersList.qml:69
msgid "installing packages"
msgstr ""

#: common/ContainersConfig.cpp:29 qml/common/ContainersList.qml:70
msgid "removing packages"
msgstr ""

#: common/ContainersConfig.cpp:30
msgid "starting"
msgstr ""

#: common/ContainersConfig.cpp:31
msgid "running"
msgstr ""

#: common/ContainersConfig.cpp:32
msgid "stopping"
msgstr ""

#: common/ContainersConfig.cpp:33
msgid "stopped"
msgstr ""

#: common/ContainersConfig.cpp:34
msgid "freezing"
msgstr ""

#: common/ContainersConfig.cpp:35
msgid "frozen"
msgstr ""

#: common/ContainersConfig.cpp:36 qml/common/ContainersList.qml:107
#: qml/common/ManageContainer.qml:193
msgid "ready"
msgstr ""

#: common/ContainersConfig.cpp:37 qml/common/ContainersList.qml:71
#: qml/common/ContainersList.qml:108 qml/common/ManageContainer.qml:131
#: qml/common/ManageContainer.qml:189
msgid "updating"
msgstr ""

#: common/ContainersConfig.cpp:38 qml/common/ContainerEditView.qml:242
#: qml/common/ContainersList.qml:68 qml/common/ExtraArchivesView.qml:69
msgid "removing"
msgstr ""

#: common/ContainersConfig.cpp:39 qml/common/ContainerInfoView.qml:159
#: qml/common/PackageInfoView.qml:102
msgid "removed"
msgstr ""

#: common/ContainersConfig.cpp:40
msgid "unknown"
msgstr ""

#: python/libertine/ChrootContainer.py:93
msgid "Unable to find correct Ubuntu Base tarball"
msgstr ""

#: python/libertine/ChrootContainer.py:100
msgid "executable bwrap not found"
msgstr ""

#: python/libertine/ChrootContainer.py:112
#, python-brace-format
msgid "Downloading {tarball_url}"
msgstr ""

#: python/libertine/ChrootContainer.py:146 python/libertine/LxcContainer.py:319
#: tools/libertine-container-manager.in:119
msgid "Failed to create container"
msgstr ""

#: python/libertine/ChrootContainer.py:157 python/libertine/LxcContainer.py:349
msgid "Adding i386 multiarch support..."
msgstr ""

#: python/libertine/ChrootContainer.py:184 python/libertine/LxcContainer.py:352
msgid "Updating the contents of the container after creation..."
msgstr ""

#: python/libertine/ChrootContainer.py:189 python/libertine/LxcContainer.py:357
#: python/libertine/LxdContainer.py:446
#, python-brace-format
msgid "Failure installing '{package_name}' during container creation"
msgstr ""

#: python/libertine/ContainerControlClient.py:41
msgid "Libraries required by the the \"libertine\" service are not installed."
msgstr ""

#: python/libertine/ContainerControlClient.py:43
#, python-brace-format
msgid "Exception raised while discovering D-Bus service: {error}"
msgstr ""

#: python/libertine/ContainersConfig.py:252
#, python-brace-format
msgid "Container id '{container_id}' does not exist."
msgstr ""

#: python/libertine/ContainersConfig.py:257
msgid "No default container available."
msgstr ""

#: python/libertine/ContainersConfig.py:301
msgid "Unable to delete container. No containers defined."
msgstr ""

#: python/libertine/HostInfo.py:76
msgid "Failed to determine the local architecture."
msgstr ""

#: python/libertine/launcher/config.py:128
msgid "Launch an application natively or in a Libertine container"
msgstr ""

#: python/libertine/launcher/config.py:130
msgid "Container identifier when launching containerized apps"
msgstr ""

#: python/libertine/launcher/config.py:135
msgid "Set an environment variable"
msgstr ""

#: python/libertine/launcher/config.py:138
msgid "exec line"
msgstr ""

#: python/libertine/launcher/config.py:142
msgid "Must specify an exec line"
msgstr ""

#: python/libertine/launcher/session.py:103
#, python-brace-format
msgid "close detected on {socket}"
msgstr ""

#: python/libertine/launcher/session.py:172
#, python-brace-format
msgid "launching {launch_task}"
msgstr ""

#: python/libertine/launcher/session.py:332
msgid "SIGCHLD received"
msgstr ""

#: python/libertine/launcher/session.py:334
msgid "launched program exited"
msgstr ""

#: python/libertine/launcher/session.py:336
msgid "SIGINT received"
msgstr ""

#: python/libertine/launcher/session.py:337
msgid "keyboard interrupt"
msgstr ""

#: python/libertine/launcher/session.py:339
msgid "SIGTERM received"
msgstr ""

#: python/libertine/launcher/session.py:340
msgid "terminate"
msgstr ""

#: python/libertine/launcher/session.py:342
#, python-brace-format
msgid "unknown signal {signal} received"
msgstr ""

#: python/libertine/Libertine.py:243
#, python-brace-format
msgid "File '{package_name}' does not exist."
msgstr ""

#: python/libertine/Libertine.py:384 python/libertine/LxcContainer.py:95
msgid "Container failed to start."
msgstr ""

#: python/libertine/Libertine.py:419
#, python-brace-format
msgid "Unsupported container type '{container_type}'"
msgstr ""

#: python/libertine/LxcContainer.py:74
#, python-brace-format
msgid "Could not open LXC log file: {logfile}"
msgstr ""

#: python/libertine/LxcContainer.py:99
msgid "Container failed to unfreeze."
msgstr ""

#: python/libertine/LxcContainer.py:103
msgid "Container failed to enter the RUNNING state."
msgstr ""

#: python/libertine/LxcContainer.py:116
msgid "Container failed to enter the FROZEN state."
msgstr ""

#: python/libertine/LxcContainer.py:121
msgid "Container failed to enter the STOPPED state."
msgstr ""

#: python/libertine/LxcContainer.py:243 python/libertine/LxdContainer.py:562
#, python-brace-format
msgid "Container '{container_id}' is not frozen. Cannot restart."
msgstr ""

#: python/libertine/LxcContainer.py:269 python/libertine/LxdContainer.py:485
msgid ""
"Canceling destruction due to running container. Use --force to override."
msgstr ""

#: python/libertine/LxcContainer.py:273
msgid "Failed to force container to stop. Canceling destruction."
msgstr ""

#: python/libertine/LxcContainer.py:325
msgid "starting container ..."
msgstr ""

#: python/libertine/LxcContainer.py:363
msgid "stopping container ..."
msgstr ""

#: python/libertine/LxdContainer.py:65
msgid ""
"Snapped libertine detected, you may need to run `sudo lxd init` manually."
msgstr ""

#: python/libertine/LxdContainer.py:95
msgid "Creating mount update shell script"
msgstr ""

#: python/libertine/LxdContainer.py:140
#, python-brace-format
msgid "Container '{container_id}' failed to start"
msgstr ""

#: python/libertine/LxdContainer.py:159
#, python-brace-format
msgid "Container '{container_id}' failed to freeze"
msgstr ""

#: python/libertine/LxdContainer.py:162
#, python-brace-format
msgid "Container '{container_id}' failed to stop"
msgstr ""

#: python/libertine/LxdContainer.py:181
#, python-brace-format
msgid "Syncing applications directory: {sync_path}"
msgstr ""

#: python/libertine/LxdContainer.py:197
#, python-brace-format
msgid "Syncing file: {container_path}:{host_path}"
msgstr ""

#: python/libertine/LxdContainer.py:233
msgid "Link mismatch while trying to fix symbolic links."
msgstr ""

#: python/libertine/LxdContainer.py:248
#, python-brace-format
msgid "Error during symlink copy of {container_path}: {error}"
msgstr ""

#: python/libertine/LxdContainer.py:262
msgid "Finding local files to remove failed."
msgstr ""

#: python/libertine/LxdContainer.py:275
msgid "Checking for missing files failed."
msgstr ""

#: python/libertine/LxdContainer.py:282
#, python-brace-format
msgid "Error while trying to remove local file {filepath}: {error}"
msgstr ""

#: python/libertine/LxdContainer.py:288
msgid "Looking for local empty directories failed."
msgstr ""

#: python/libertine/LxdContainer.py:334
#, python-brace-format
msgid "Bind-mount path '{mount_path}' does not exist."
msgstr ""

#: python/libertine/LxdContainer.py:353
#, python-brace-format
msgid "Saving bind mounts for container '{container_id}' raised:"
msgstr ""

#: python/libertine/LxdContainer.py:372
msgid "Updating existing lxd profile."
msgstr ""

#: python/libertine/LxdContainer.py:376
msgid "Saving libertine lxd profile raised:"
msgstr ""

#: python/libertine/LxdContainer.py:378
msgid "Creating libertine lxd profile."
msgstr ""

#: python/libertine/LxdContainer.py:402
msgid "Container already exists"
msgstr ""

#: python/libertine/LxdContainer.py:407
#, python-brace-format
msgid "Creating container '{container_id}' with distro '{container_distro}'"
msgstr ""

#: python/libertine/LxdContainer.py:413
#, python-brace-format
msgid "Creating container '{container_id}' failed with code '{error_code}'"
msgstr ""

#: python/libertine/LxdContainer.py:437
#, python-brace-format
msgid "Adding i386 multiarch support to container '{container_id}'"
msgstr ""

#: python/libertine/LxdContainer.py:443
#, python-brace-format
msgid "Installing package '{package_name}' in container '{container_id}'"
msgstr ""

#: python/libertine/LxdContainer.py:468
msgid "Re-syncing timezones"
msgstr ""

#: python/libertine/LxdContainer.py:481
#, python-brace-format
msgid "No such container '{container_id}'"
msgstr ""

#: python/libertine/LxdContainer.py:533
#, python-brace-format
msgid "Network unavailable in container '{container_id}'"
msgstr ""

#: python/libertine/LxdContainer.py:572
#, python-brace-format
msgid "Could not get container '{container_id}'"
msgstr ""

#: python/libertine/service/container_control_client.py:43
#, python-brace-format
msgid "Container app '{application_name}' is not valid."
msgstr ""

#: python/libertine/service/container.py:35
msgid "Using AptCache not currently supported in snap environment"
msgstr ""

#: python/libertine/utils.py.in:170
#, python-brace-format
msgid "Exception caught while setting session dbus address: {error}"
msgstr ""

#: qml/common/AddExtraArchiveView.qml:27
msgid "Add Archive"
msgstr ""

#: qml/common/AddExtraArchiveView.qml:44
msgid "New archive identifier, e.g."
msgstr ""

#: qml/common/AddExtraArchiveView.qml:52
msgid ""
"multiverse\n"
"ppa:user/repository\n"
"deb http(s)://myserver/repo stable repo"
msgstr ""

#: qml/common/AddExtraArchiveView.qml:75
msgid "(Optional) Public signing-key for archive"
msgstr ""

#: qml/common/AddExtraArchiveView.qml:92
msgid "Add"
msgstr ""

#: qml/common/ContainerEditView.qml:30
msgid "%1 - All Apps"
msgstr ""

#: qml/common/ContainerEditView.qml:49 qml/common/ContainerEditView.qml:133
msgid "Install new package"
msgstr ""

#: qml/common/ContainerEditView.qml:50
msgid "Enter exact package name or full path to a Debian package file"
msgstr ""

#: qml/common/ContainerEditView.qml:60
msgid "Package name or Debian package path"
msgstr ""

#: qml/common/ContainerEditView.qml:70 qml/common/ContainerEditView.qml:122
#: qml/common/ContainerEditView.qml:164
#: qml/common/ContainerOptionsDialog.qml:80
#: qml/common/SearchPackagesDialog.qml:45
msgid "Cancel"
msgstr ""

#: qml/common/ContainerEditView.qml:77
msgid "Install"
msgstr ""

#: qml/common/ContainerEditView.qml:87
msgid ""
"The %1 package is already installed. Please try a different package name."
msgstr ""

#: qml/common/ContainerEditView.qml:104
msgid "Container Actions"
msgstr ""

#: qml/common/ContainerEditView.qml:107
msgid "Manage Container"
msgstr ""

#: qml/common/ContainerEditView.qml:115
msgid "Container Information"
msgstr ""

#: qml/common/ContainerEditView.qml:134
msgid "Choose a package installation method"
msgstr ""

#: qml/common/ContainerEditView.qml:138
msgid "Enter package name or Debian file"
msgstr ""

#: qml/common/ContainerEditView.qml:147
msgid "Choose Debian package to install"
msgstr ""

#: qml/common/ContainerEditView.qml:156
msgid "Search archives for a package"
msgstr ""

#: qml/common/ContainerEditView.qml:254 qml/common/ContainersList.qml:81
msgid "delete"
msgstr ""

#: qml/common/ContainerEditView.qml:255
msgid "Remove Package"
msgstr ""

#: qml/common/ContainerEditView.qml:266 qml/common/ContainersList.qml:96
msgid "info"
msgstr ""

#: qml/common/ContainerEditView.qml:267
msgid "Package Info"
msgstr ""

#: qml/common/ContainerEditView.qml:284
msgid "No packages are installed"
msgstr ""

#: qml/common/ContainerInfoView.qml:30
msgid "Container Info: %1"
msgstr ""

#: qml/common/ContainerInfoView.qml:58
msgid "ID"
msgstr ""

#: qml/common/ContainerInfoView.qml:66
msgid "Name"
msgstr ""

#: qml/common/ContainerInfoView.qml:74
msgid "Distribution"
msgstr ""

#: qml/common/ContainerInfoView.qml:82
msgid "Status"
msgstr ""

#: qml/common/ContainerInfoView.qml:92
msgid "Hide"
msgstr ""

#: qml/common/ContainerInfoView.qml:92
msgid "Show"
msgstr ""

#: qml/common/ContainerInfoView.qml:93 qml/common/PackageInfoView.qml:74
msgid "None"
msgstr ""

#: qml/common/ContainerInfoView.qml:99 qml/common/PackageInfoView.qml:80
msgid "Operation details"
msgstr ""

#: qml/common/ContainerOptionsDialog.qml:27
msgid "Container Options"
msgstr ""

#: qml/common/ContainerOptionsDialog.qml:28
msgid "Configure options for container creation."
msgstr ""

#: qml/common/ContainerOptionsDialog.qml:41 qml/common/ManageContainer.qml:71
msgid "i386 multiarch support"
msgstr ""

#: qml/common/ContainerOptionsDialog.qml:51
msgid "Enter a name for the container or leave blank for default name:"
msgstr ""

#: qml/common/ContainerOptionsDialog.qml:57
msgid "container name"
msgstr ""

#: qml/common/ContainerOptionsDialog.qml:64
#: tools/libertine-container-manager.in:101
msgid ""
"Enter password for your user in the Libertine container or leave blank for "
"no password:"
msgstr ""

#: qml/common/ContainerOptionsDialog.qml:71
msgid "password"
msgstr ""

#: qml/common/ContainerOptionsDialog.qml:87
msgid "OK"
msgstr ""

#: qml/common/ContainersList.qml:43 qml/common/ContainersList.qml:170
msgid "Container Unavailable"
msgstr ""

#: qml/common/ContainersList.qml:43
msgid "Container is being destroyed and is no longer editable."
msgstr ""

#: qml/common/ContainersList.qml:82
msgid "Delete Container"
msgstr ""

#: qml/common/ContainersList.qml:97
msgid "Container Info"
msgstr ""

#: qml/common/ContainersList.qml:105
msgid "edit"
msgstr ""

#: qml/common/ContainersList.qml:106
msgid "Container Apps"
msgstr ""

#: qml/common/ContainersList.qml:131
msgid "Welcome to Libertine - the X11 Sandbox Application Manager."
msgstr ""

#: qml/common/ContainersList.qml:139
msgid ""
"You do not have Classic Application Support configured at this time. "
"Downloading and setting up the required environment may take some time and "
"network bandwidth."
msgstr ""

#: qml/common/ContainersList.qml:146
msgid "Get started"
msgstr ""

#: qml/common/ContainersList.qml:171
msgid ""
"This container has been destroyed and is no longer valid. You have been "
"returned to the containers overview."
msgstr ""

#: qml/common/DebianPackagePicker.qml:28
msgid "Available Debian Packages to Install"
msgstr ""

#: qml/common/DebianPackagePicker.qml:71 qml/common/SearchResults.qml:63
msgid "Install Package"
msgstr ""

#: qml/common/DebianPackagePicker.qml:88
msgid "No Debian packages available"
msgstr ""

#: qml/common/ExtraArchivesView.qml:28
msgid "Additional Archives"
msgstr ""

#: qml/common/ExtraArchivesView.qml:32 qml/common/ExtraBindMountsView.qml:28
msgid "add"
msgstr ""

#: qml/common/ExtraArchivesView.qml:33
msgid "Add a new archive"
msgstr ""

#: qml/common/ExtraArchivesView.qml:76 qml/common/ExtraBindMountsView.qml:64
msgid "remove"
msgstr ""

#: qml/common/ExtraArchivesView.qml:77
msgid "Remove extra archive"
msgstr ""

#: qml/common/ExtraArchivesView.qml:94
msgid "No additional archives or PPAs have been added"
msgstr ""

#: qml/common/ExtraBindMountsView.qml:24
msgid "Mapped Directories"
msgstr ""

#: qml/common/ExtraBindMountsView.qml:29
msgid "Add a new bind-mount"
msgstr ""

#: qml/common/ExtraBindMountsView.qml:65
msgid "Remove mapped directory"
msgstr ""

#: qml/common/ExtraBindMountsView.qml:82
msgid ""
"No custom bind-mounts have been added to this container.\n"
"Adding a directory here will allow you to modify its contents within this "
"container."
msgstr ""

#: qml/common/GenericErrorDialog.qml:39
msgid "Dismiss"
msgstr ""

#: qml/common/GenericErrorDialog.qml:45
msgid "Copy to Clipboard"
msgstr ""

#: qml/common/ManageContainer.qml:29
msgid "Manage %1"
msgstr ""

#: qml/common/ManageContainer.qml:95
msgid "Freeze on stop"
msgstr ""

#: qml/common/ManageContainer.qml:99
msgid "Additional archives and PPAs"
msgstr ""

#: qml/common/ManageContainer.qml:108 tools/libertine-container-manager.in:620
msgid "Additional bind-mounts"
msgstr ""

#: qml/common/ManageContainer.qml:119
msgid "Update…"
msgstr ""

#: qml/common/ManageContainer.qml:134
msgid "Update container"
msgstr ""

#: qml/common/ManageContainer.qml:152
msgid "Default container"
msgstr ""

#: qml/common/PackageExistsDialog.qml:28
msgid "The %1 package is already installed."
msgstr ""

#: qml/common/PackageExistsDialog.qml:29
msgid "Search again or return to search results."
msgstr ""

#: qml/common/PackageExistsDialog.qml:33
msgid "Search again"
msgstr ""

#: qml/common/PackageExistsDialog.qml:43
msgid "Return to search results"
msgstr ""

#: qml/common/PackageInfoView.qml:30
msgid "%1 - %2"
msgstr ""

#: qml/common/PackageInfoView.qml:35
msgid "Obtaining package version…"
msgstr ""

#: qml/common/PackageInfoView.qml:55
msgid "Package version"
msgstr ""

#: qml/common/PackageInfoView.qml:63
msgid "Install status"
msgstr ""

#: qml/common/PackageInfoView.qml:73
msgid "View"
msgstr ""

#: qml/common/PackageInfoView.qml:113
msgid "Unknown"
msgstr ""

#: qml/common/SearchPackagesDialog.qml:26
msgid "Search for a package"
msgstr ""

#: qml/common/SearchPackagesDialog.qml:27
msgid "Search for a package in the archives by name"
msgstr ""

#: qml/common/SearchPackagesDialog.qml:35
#: qml/common/SearchPackagesDialog.qml:54 qml/common/SearchResultsView.qml:34
msgid "Search"
msgstr ""

#: qml/common/SearchResultsView.qml:30
msgid "Package Search Results"
msgstr ""

#: qml/common/SearchResultsView.qml:35
msgid "Search for packages"
msgstr ""

#: qml/common/SearchResultsView.qml:54
msgid "No Search Results Found"
msgstr ""

#: qml/common/SearchResultsView.qml:59
msgid "Search Again"
msgstr ""

#: qml/common/SearchResultsView.qml:69
msgid "Return to Apps Page"
msgstr ""

#: qml/common/SearchResultsView.qml:98
msgid "Searching for packages…"
msgstr ""

#: qml/gui/ContainersView.qml:35
msgid "My Containers"
msgstr ""

#: qml/plugin/MainSettingsPage.qml:30
msgid "Manage Libertine Containers"
msgstr ""

#: tools/libertine-container-manager.in:46 tools/libertine-launch.in:42
#, python-brace-format
msgid ""
"Backend for container '{id}' not installed. Install 'python3-libertine-"
"{type}' and try again."
msgstr ""

#: tools/libertine-container-manager.in:62
#, python-brace-format
msgid "Invalid distro {distro}"
msgstr ""

#: tools/libertine-container-manager.in:66
#, python-brace-format
msgid "Container id '{container_id}' is already used."
msgstr ""

#: tools/libertine-container-manager.in:69
#, python-brace-format
msgid ""
"Container id '{container_id}' invalid. ID must be of form ([a-z0-9][a-"
"z0-9+.-]+)."
msgstr ""

#: tools/libertine-container-manager.in:78
#, python-brace-format
msgid ""
"System kernel does not support {container_type} type containers. Please "
"either use chroot or omit the -t option."
msgstr ""

#: tools/libertine-container-manager.in:89
#, python-brace-format
msgid ""
"The container distribution needs to match the host distribution for chroot "
"based containers. Please either use '{host_distro}' or omit the -d/--distro "
"option."
msgstr ""

#: tools/libertine-container-manager.in:126
#, python-brace-format
msgid "Failed to create container: '{error}'"
msgstr ""

#: tools/libertine-container-manager.in:168
#, python-brace-format
msgid "{package_name} does not exist."
msgstr ""

#: tools/libertine-container-manager.in:176
#, python-brace-format
msgid "Package '{package_name}' is already installed."
msgstr ""

#: tools/libertine-container-manager.in:184
#, python-brace-format
msgid ""
"Package '{package_name}' failed to install in container '{container_id}'"
msgstr ""

#: tools/libertine-container-manager.in:219
#, python-brace-format
msgid "Package '{package_name}' is not installed."
msgstr ""

#: tools/libertine-container-manager.in:224
#, python-brace-format
msgid ""
"Package '{package_name}' failed to be removed from container '{container_id}'"
msgstr ""

#: tools/libertine-container-manager.in:237
#, python-brace-format
msgid ""
"Search for '{query_string}' in container '{container_id}' exited with non-"
"zero status"
msgstr ""

#: tools/libertine-container-manager.in:296
#, python-brace-format
msgid "i386 multiarch support is already {enabled_or_disabled}"
msgstr ""

#: tools/libertine-container-manager.in:306
msgid ""
"Configure archive called with no archive name. See configure --help for "
"usage."
msgstr ""

#: tools/libertine-container-manager.in:314
#, python-brace-format
msgid "{archive_name} already added in container."
msgstr ""

#: tools/libertine-container-manager.in:327
#, python-brace-format
msgid "{archive_name} is not added in container."
msgstr ""

#: tools/libertine-container-manager.in:332
#, python-brace-format
msgid "{archive_name} was not properly deleted."
msgstr ""

#: tools/libertine-container-manager.in:337
msgid ""
"Configure bind-mounts called without mount path. See configure --help for "
"usage"
msgstr ""

#: tools/libertine-container-manager.in:344
#, python-brace-format
msgid ""
"Cannot mount {mount_path}, mount path must be in {home_dir} or /media/"
"{username}."
msgstr ""

#: tools/libertine-container-manager.in:350
msgid "/media mounts not currently supported in lxc."
msgstr ""

#: tools/libertine-container-manager.in:353
#, python-brace-format
msgid "Cannot mount '{mount_path}', mount path must be an existing directory."
msgstr ""

#: tools/libertine-container-manager.in:361
#, python-brace-format
msgid "Cannot add mount '{mount_path}', bind-mount already exists."
msgstr ""

#: tools/libertine-container-manager.in:367
#, python-brace-format
msgid "Cannot remove mount '{mount_path}', bind-mount does not exist."
msgstr ""

#: tools/libertine-container-manager.in:377
msgid ""
"Container cannot be restarted at this time.  You will need to restart the "
"container at a later time using the 'restart' subcommand."
msgstr ""

#: tools/libertine-container-manager.in:384
msgid "Configuring freeze is only valid on LXC and LXD container types."
msgstr ""

#: tools/libertine-container-manager.in:390
msgid "Configure called with no subcommand. See configure --help for usage."
msgstr ""

#: tools/libertine-container-manager.in:431
msgid "The restart subcommand is only valid for LXC and LXD type containers."
msgstr ""

#: tools/libertine-container-manager.in:440
msgid "Classic X application support for Lomiri"
msgstr ""

#: tools/libertine-container-manager.in:446 tools/libertined.in:39
msgid "disables all non-vital output"
msgstr ""

#: tools/libertine-container-manager.in:449 tools/libertined.in:42
msgid "enables debug output"
msgstr ""

#: tools/libertine-container-manager.in:457
msgid "Create a new Libertine container."
msgstr ""

#: tools/libertine-container-manager.in:461
msgid "Container identifier of form ([a-z0-9][a-z0-9+.-]+). Required."
msgstr ""

#: tools/libertine-container-manager.in:464
msgid "Type of Libertine container to create. Either 'lxd', 'lxc' or 'chroot'."
msgstr ""

#: tools/libertine-container-manager.in:467
msgid "Ubuntu distro series to create."
msgstr ""

#: tools/libertine-container-manager.in:470
msgid "User friendly container name."
msgstr ""

#: tools/libertine-container-manager.in:473
msgid ""
"Force the installation of the given valid Ubuntu distro even if it is no "
"longer supported."
msgstr ""

#: tools/libertine-container-manager.in:477
msgid ""
"Add i386 support to amd64 Libertine containers.  This option has no effect "
"when the Libertine container is i386."
msgstr ""

#: tools/libertine-container-manager.in:481
msgid ""
"Pass in the user's password when creating an LXC container.  This is "
"intended for testing only and is very insecure."
msgstr ""

#: tools/libertine-container-manager.in:488
msgid "Destroy any existing environment entirely."
msgstr ""

#: tools/libertine-container-manager.in:491
#: tools/libertine-container-manager.in:509
#: tools/libertine-container-manager.in:527
#: tools/libertine-container-manager.in:543
#: tools/libertine-container-manager.in:553
#: tools/libertine-container-manager.in:568
#: tools/libertine-container-manager.in:582
#: tools/libertine-container-manager.in:594
#: tools/libertine-container-manager.in:663
#: tools/libertine-container-manager.in:676
msgid "Container identifier.  Default container is used if omitted."
msgstr ""

#: tools/libertine-container-manager.in:494
msgid "Force destroy.  Forces running containers to stop before destruction."
msgstr ""

#: tools/libertine-container-manager.in:500
msgid "Install a package or packages in the specified Libertine container."
msgstr ""

#: tools/libertine-container-manager.in:505
msgid ""
"Name of package or full path to a Debian package. Multiple packages can be "
"entered, separated by a space. Required."
msgstr ""

#: tools/libertine-container-manager.in:512
#: tools/libertine-container-manager.in:530
msgid "No dialog mode. Use text-based frontend during debconf interactions."
msgstr ""

#: tools/libertine-container-manager.in:518
msgid "Remove a package in the specified Libertine container."
msgstr ""

#: tools/libertine-container-manager.in:523
msgid ""
"Name of package to remove. Multiple packages can be entered, separated by a "
"space. Required."
msgstr ""

#: tools/libertine-container-manager.in:536
msgid ""
"Search for packages based on the search string in the specified Libertine "
"container."
msgstr ""

#: tools/libertine-container-manager.in:540
msgid "String to search for in the package cache. Required."
msgstr ""

#: tools/libertine-container-manager.in:549
msgid ""
"Update the packages in the Libertine container.  Also updates the "
"container's locale and installs necessary language packs if the host's "
"locale has changed."
msgstr ""

#: tools/libertine-container-manager.in:559
msgid "List all Libertine containers."
msgstr ""

#: tools/libertine-container-manager.in:565
msgid "List available app launchers in a container."
msgstr ""

#: tools/libertine-container-manager.in:572
msgid "use JSON output format."
msgstr ""

#: tools/libertine-container-manager.in:585
msgid "The command to run in the specified container."
msgstr ""

#: tools/libertine-container-manager.in:591
msgid "Configure various options in the specified Libertine container."
msgstr ""

#: tools/libertine-container-manager.in:595
msgid "Multiarch support"
msgstr ""

#: tools/libertine-container-manager.in:596
msgid "Enable or disable multiarch support for a container."
msgstr ""

#: tools/libertine-container-manager.in:600
msgid ""
"Enables or disables i386 multiarch support for amd64 Libertine containers. "
"This option has no effect when the Libertine container is i386."
msgstr ""

#: tools/libertine-container-manager.in:604
msgid "Additional archive support"
msgstr ""

#: tools/libertine-container-manager.in:605
msgid "Add or delete an additional archive (PPA)."
msgstr ""

#: tools/libertine-container-manager.in:609
msgid "Adds or removes an archive (PPA) in the specified Libertine container."
msgstr ""

#: tools/libertine-container-manager.in:612
msgid "Archive name"
msgstr ""

#: tools/libertine-container-manager.in:613
msgid "Archive name to be added or removed."
msgstr ""

#: tools/libertine-container-manager.in:616
msgid "Public key file"
msgstr ""

#: tools/libertine-container-manager.in:617
msgid ""
"File containing the key used to sign the given archive. Useful for third-"
"party or private archives."
msgstr ""

#: tools/libertine-container-manager.in:621
msgid "Add or delete an additional bind-mount."
msgstr ""

#: tools/libertine-container-manager.in:625
msgid "Adds or removes a bind-mount in the specified Libertine container."
msgstr ""

#: tools/libertine-container-manager.in:628
msgid "Mount path"
msgstr ""

#: tools/libertine-container-manager.in:629
msgid "The absolute host path to bind-mount."
msgstr ""

#: tools/libertine-container-manager.in:631
msgid "Freeze container support"
msgstr ""

#: tools/libertine-container-manager.in:632
msgid "Enable or disable freezing LXC/LXD containers when not in use."
msgstr ""

#: tools/libertine-container-manager.in:636
msgid ""
"Enables or disables freezing of LXC/LXD containers when not in use. When "
"disabled, the container will stop."
msgstr ""

#: tools/libertine-container-manager.in:659
msgid "Set the default container."
msgstr ""

#: tools/libertine-container-manager.in:662
msgid "Container id"
msgstr ""

#: tools/libertine-container-manager.in:666
msgid "Clear the default container."
msgstr ""

#: tools/libertine-container-manager.in:672
msgid ""
"Restart a frozen Libertine container.  This only works on LXC and LXD type "
"containers."
msgstr ""

#: tools/libertine-container-manager.in:683
#, python-brace-format
msgid "Please do not run '{program_name}' using sudo"
msgstr ""

#: tools/libertine-launch.in:34
#, python-brace-format
msgid "No container with id '{container_id}'"
msgstr ""

#: tools/libertine-shell.in:41
msgid "Launch an SSH session within a lxc/lxd Libertine container"
msgstr ""

#: tools/libertine-shell.in:43
msgid "Container identifier"
msgstr ""

#: tools/libertine-shell.in:45
msgid "Container username"
msgstr ""

#: tools/libertine-shell.in:47
msgid "SSH key to be used"
msgstr ""

#: tools/libertine-shell.in:50
msgid "Assume yes to all prompts"
msgstr ""

#: tools/libertine-shell.in:72
msgid "Identity file not found at '{}'. Leave blank for default."
msgstr ""

#: tools/libertine-shell.in:78
msgid "Corresponding public key not found for '{}'."
msgstr ""

#: tools/libertine-shell.in:111
msgid "Configured identity file or public key matching '{}' do not exist."
msgstr ""

#: tools/libertine-shell.in:139
msgid ""
"It looks like no SSH keys are set up. Please generate a key and try again. "
"You can use the following command to generate an appropriate key:\n"
"\tssh-keygen -t rsa -b 4096 -C 'your_email@example.com'"
msgstr ""

#: tools/libertine-shell.in:160
msgid ""
"Always use '{}' as identity file and username '{}' when connecting to '{}'? "
"[Yn]"
msgstr ""

#: tools/libertine-shell.in:163
msgid "Always use '{}' as identity file when connecting to '{}'? [Yn]"
msgstr ""

#: tools/libertine-shell.in:165 tools/libertine-shell.in:209
#: tools/libertine-shell.in:243
msgid "Y"
msgstr ""

#: tools/libertine-shell.in:165 tools/libertine-shell.in:209
#: tools/libertine-shell.in:243
msgid "y"
msgstr ""

#: tools/libertine-shell.in:190
msgid ""
"No sshd found. You can install openssh with the following command:\n"
"\tapt install openssh-client"
msgstr ""

#: tools/libertine-shell.in:197
msgid ""
"'{}' is a '{}' container. Only 'lxd' or 'lxc' containers are able to use "
"this tool."
msgstr ""

#: tools/libertine-shell.in:208
msgid "openssh-server not detected in container '{}'. Install now? [Yn]"
msgstr ""

#: tools/libertine-shell.in:214
msgid "Failed to install openssh-server"
msgstr ""

#: tools/libertine-shell.in:220 tools/libertine-shell.in:227
msgid "Unable to get IP address for '{}'"
msgstr ""

#: tools/libertine-shell.in:242
msgid "OK to add public key '{}' to container '{}'? [Yn]"
msgstr ""

#: tools/libertine-shell.in:244
msgid "Public key must be added to container to continue."
msgstr ""

#: tools/libertine-shell.in:248
msgid "Failed to add public key to container's authorized keys."
msgstr ""

#: tools/libertined.in:36
msgid "Libertine Store service"
msgstr ""

#: tools/libertined.in:60
msgid "shutting service down"
msgstr ""

#: tools/libertined.in:71
msgid "Initializing libertined..."
msgstr ""

#: tools/libertined.in:79
msgid "service is already running"
msgstr ""

#: tools/libertined.in:87
msgid "libertined ready"
msgstr ""

#: tools/libertined.in:92
#, python-brace-format
msgid "Unexpected exception occurred: '{error}'"
msgstr ""
